package com.springdatabase.onetoone.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;


import com.springdatabase.onetoone.Entity.User;
import com.springdatabase.onetoone.Repository.UserRepository;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;


@Service
public class UserService {
	@Autowired
	  private AllUserDataFetcher allUserDataFetcher;
	@Autowired
	private UserRepository repository;
	
	public User saveUser(User user) {
		user = repository.save(user);
		return user;
	}
	public List<User> saveUsers(List<User> users) {
		return repository.saveAll(users);
	}
	public List<User> getUsers() {
		return repository.findAll();
	}
	public User getUsersById(Long userId) {
		User user = repository.findById(userId).orElse(null);
		return user;
	}
	public String deleteProduct(Long id) {
		repository.deleteById(id);
		return "User ID "+ id + " removed ";
	}
	
	@Autowired
	
	  @Value("classpath:onetoone.graphql")
	  Resource resource;
	  private GraphQL graphQL;

	  @PostConstruct
	  private void loadSchema() throws IOException {

	    File file = resource.getFile();

	    // Get the graphql file
	    TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(file);
	    RuntimeWiring runtimeWiring = buildRuntimeWiring();
	    GraphQLSchema graphQLSchema
	      = new SchemaGenerator().makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);
	graphQL = GraphQL.newGraphQL(graphQLSchema).build();
	  }

	  private RuntimeWiring buildRuntimeWiring() {
	    return RuntimeWiring.newRuntimeWiring()
	      .type("Query",
	        typeWiring -> typeWiring
	        .dataFetcher("allUsers", allUserDataFetcher))
	      .build();
	  }

	  public ExecutionResult executeGraphQL(String query) {
	    return graphQL.execute(query);
	  }
	  

}