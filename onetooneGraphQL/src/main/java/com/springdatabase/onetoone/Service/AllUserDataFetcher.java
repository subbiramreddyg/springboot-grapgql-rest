package com.springdatabase.onetoone.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.springdatabase.onetoone.Entity.User;
import com.springdatabase.onetoone.Repository.UserRepository;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
@Component
public class AllUserDataFetcher implements DataFetcher<List<User>>{
	@Autowired
	  private UserRepository repository;
@Override
	  public List<User> get(DataFetchingEnvironment environment) {
	    return repository.findAll();
	  }
	}

